
#include <stdbool.h>
#include <stdio.h>
#include <assert.h>
#include "board.c"
#include "libothello.h"


unsigned int is_valid(Board board, unsigned int x, unsigned int y, unsigned int player) {
	//is the move inside the board?
	if (x>7 || y>7 || x<0 || y<0) {
		return false;
	//is the move on top of another piece
	} else if (!is_player(board, x,y,B_NULL)) {
		return false;
	} else {
		return true;
	}
}

/* algorithm description:
 *check for basic validity:
 *	is the move inside the board
 *	is the move on top of another piece
 *	if valid, set that piece on the board temporarily
 *for each direction in directions(8 directions)
 *	traverse in that direction by 1 cell
 *	if cell is the opposite player, mark that
 *	if cell is the same player, move to next direction
 *	if cell is whitespace, move to next direction
 *	if we found the oposite player:
 *		traverse in that direction by 1 cell until we either hit a wall or whitespace or find the same player's piece
 *		if we found the same player's piece:
 *			traverse backwards until we reach our starting point, incrementing the return value for each piece flipped
 *	if the return value is zero:
 *		unset the previous temporary move
 *	return return value
*/

bool is_oob(unsigned int x,unsigned int y) {
	return (x<0 || y<0 || x>7 || y>7);
}

//the code below is devoted to this algorithm
typedef struct direction {
	unsigned int x;
	unsigned int y;
} Direction;

Direction new_direction() {
	Direction direction;
	direction.x = -2;
	direction.y = -2;
	return direction;
}

bool inc_direction(Direction *d) {
	if (d->x == -2) {
		d->x=-1;
		d->y=-1;
		return true;
	}
	if (d->x == 1 && d->y == 1) {
		return false;
	} else if (d->x == 1) {
		d->x = -1;
		d->y++;
	} else {
		d->x++;
	}
	return true;
}

void move_direction(Direction direction, unsigned int *x, unsigned int *y) {
	*x+=direction.x;
	*y+=direction.y;
	return;
}

Direction reverse_direction(Direction direction) {
	Direction r_dir = direction;
	r_dir.x=-r_dir.x;
	r_dir.y=-r_dir.y;
	return r_dir;
}


unsigned int exec_move(Board *board, unsigned int x, unsigned int y, unsigned int player) {
	//broken
	unsigned int s_x = x;
	unsigned int s_y = y;
	//check for basic validity:
	if (!is_valid(*board,x,y,player)) {
		return 0;
	}
	Direction direction = new_direction();
	unsigned int opposite_player = player^F_SWITCHPLAYER;
	unsigned int retval = 0;

	//for each direction in directions(8 directions)
	while (inc_direction(&direction)) {
		bool found_opposite_player = false;
		unsigned int n_of_moves = 0;
		bool stopped = false;
		while (!stopped) {
			//traverse in that direction by 1 cell
			move_direction(direction,&x,&y);
			n_of_moves++;
			if (is_oob(x,y)) {
				stopped = true;
			//if cell is the opposite player, mark that
			} else if (is_player(*board, x,y,opposite_player)) {
				found_opposite_player = true;
			//if cell is the same player, move to next direction
			} else if (is_player(*board,x,y,player) && !found_opposite_player) {
				stopped = true;
			//if cell is whitespace, move to next direction
			} else if (is_player(*board,x,y,B_NULL)) {
				stopped = true;
			} else if (is_player(*board,x,y,player) && found_opposite_player) {
				Direction r_dir = reverse_direction(direction);
				//traverse backwards
				while (n_of_moves != 0) {
					move_direction(r_dir,&x,&y);
					set_piece_at(board,x,y,player);
					n_of_moves--;
					retval++;
				}
				assert(n_of_moves == 0);
				stopped = true;
			}
		}
		x=s_x;
		y=s_y;
	}
	assert(x == s_x);
	assert(y == s_y);
	return retval;
}


ExecMoveNomutRetval exec_move_nomut(Board board, unsigned int x, unsigned int y, unsigned int player) {
	Board newboard;
	memcpy(&newboard,&board,sizeof(Board));
	unsigned int retval = exec_move(&newboard,x,y,player);
	ExecMoveNomutRetval rv;
	rv.retval = retval;
	rv.board=newboard;
	return rv;
}

bool valid_move_exists(Board board, unsigned int player) {
	unsigned int i = 0;
	while (i<8*8) {
		unsigned int x = i%8;
		unsigned int y = i/8;
		if (is_player(board,x,y,B_NULL)) {
			ExecMoveNomutRetval rv = exec_move_nomut(board,x,y,player);
			if (rv.retval != 0) {
				return true;
			}
		}
		i++;
	}
	return false;
}

CountPiecesRetval count_pieces(Board board) {
	unsigned int i = 0;
	unsigned int x = 0;
	unsigned int o = 0;
	while (i<8*8) {
		unsigned int lx = i%8;
		unsigned int ly = i/8;
		if (is_player(board,lx,ly,B_PLAYER1)) {
			x++;
		} else if (is_player(board,lx,ly,B_PLAYER2)) {
			o++;
		}
		i++;
	}
	CountPiecesRetval rv;
	rv.x = x;
	rv.o = o;
	return rv;
}
