#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(dead_code)]

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

use crate::GameState;
use std::fmt;
use std::str::FromStr;
use std::num::ParseIntError;

impl Coordinate {
    ///Creates a coordinate with location outside the board
    pub fn null() -> Self {
        return Coordinate{x: u32::MAX,y: u32::MAX};
    }
    ///Creates a new coordinate set using x and y, !!!Does not translate screen coordinates (1,1)
    ///to in-memory coordinates (0,0)
    pub fn new(x: u32, y: u32) -> Self {
        return Coordinate{x,y};
    }
}

impl PartialEq for Coordinate {
    fn eq(&self, other: &Self) -> bool {
        self.x == other.x &&  self.y == other.y
    }
}
///Translates "(1,1)"->(0,0)
impl FromStr for Coordinate {
    type Err = ParseIntError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        //copied from rust-lang doc
        let coords: Vec<&str> = s.trim_matches(|p| p == '(' || p == ')' )
                         .split(',')
                         .collect();

        let x_fromstr = coords[0].parse::<u32>()?;
        let y_fromstr = coords[1].parse::<u32>()?;

        Ok(Coordinate { x: x_fromstr-1, y: y_fromstr-1})
    }
}

impl Eq for Coordinate {}

impl PartialEq for Board {
    fn eq(&self, other: &Self) -> bool {
        let mut i = 0;
        for cell in self.cells {
            if cell != other.cells[i] {
                return false;
            }
            i+=1;
        }
        return true;
    }
}

impl Eq for Board {}

impl fmt::Display for Board {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        //this function is translated from ui.c's redraw_board() function
	//for each FourCell in Board
	write!(f,"x 1 2 3 4 5 6 7 8  y\n ")?;

	for i in 0..64 {
		let x=i%8;
		let y=i/8;
		write!(f,"|")?;
		if self.is_player(x,y,B_PLAYER1) {
			write!(f,"{}", C_PLAYER1 as char)?;
		} else if self.is_player(x,y,B_PLAYER2) {
			write!(f,"{}",C_PLAYER2 as char)?;
		} else if self.is_player(x,y,B_NULL) {
			write!(f," ")?;
		} else {
			write!(f,"e")?;
		}
		//every two FourCells, print a newline
		if x == 7 {
			write!(f,"| {}\n ",i/8+1)?;
		}
	}
	write!(f,"\n")
    }
}

impl fmt::Display for Coordinate {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if *self == Coordinate::null() {
            write!(f,"(?,?)")
        } else {
            write!(f, "({},{})", self.x+1,self.y+1)
        }
    }
}

impl Board {
    ///creates a board with the default othello positions
    pub fn new() -> Self {
        return unsafe {new_board()};
    }
    ///Creates an empty board
    pub fn empty() -> Self {
        return unsafe {empty_board()};
    }
    ///Executes a move on a copy of the board, returns the number of pieces flipped and the
    ///new board
    pub fn exec_move_nomut(&self, x: u32, y: u32, player: u32) -> (GameState,u32) {
        let rv = unsafe {exec_move_nomut(*self,x,y,player)};
        return (GameState::new(rv.board, player),rv.retval);
    }
    ///Executes a move on the board
    pub fn exec_move(&mut self, x: u32, y: u32, player: u32) -> u32 {
        return unsafe {exec_move(self,x,y,player)};
    }
    ///Checks if position at (x,y) on self is player
    pub fn is_player(&self, x: u32, y: u32, player: u32) -> bool {
        return unsafe {is_player(*self, x,y,player)};
    }
    ///Sets piece at (x,y) to player
    pub fn set_piece_at(&mut self, x: u32, y: u32, player: u32) {
        return unsafe {set_piece_at(self, x,y,player)};
    }
    ///Returns a FourCell containing player info at (x,y)
    pub fn get_single_cell(&self, x: u32, y:u32) -> u32 {
        return unsafe {get_single_cell(*self,x,y)};
    }
    ///Converts a board in a string into a Board.
    ///Board must be in a specific configuration, see tests for examples
    pub fn from_str(s: &str) -> Self {
        let s = String::from(s);
        let mut board = Board::empty();
        let mut i = 0;
        for ch in s.chars() {
            if ch == ' ' {
                board.set_piece_at(i%8,i/8,B_NULL);
                i+=1;
            } else if ch == 'x' {
                board.set_piece_at(i%8,i/8,B_PLAYER1);
                i+=1;
            } else if ch == 'o' {
                board.set_piece_at(i%8,i/8,B_PLAYER2);
                i+=1;
            }
        }
        return board;
    }
    ///Checks if player can make a valid move
    pub fn valid_move_exists(&self,player: u32) -> bool {
        return unsafe {valid_move_exists(*self,player)};
    }
    ///Counts the number of pieces on the board and returns (count x, count 0)
    pub fn count_pieces(&self) -> (u8,u8) {
        let cpr = unsafe {count_pieces(*self)};
        return (cpr.x as u8,cpr.o as u8);
    } 
}

///Returns individual cells
pub struct BoardIter {
    board: Board,
    counter: u32,
}

///Iterates over cells
impl Iterator for BoardIter {
    type Item = u32;
    fn next(&mut self) -> Option<Self::Item> {
        if self.counter >= 8*8 {
            return None;
        } else {
            //x,y
            let c = self.counter;
            self.counter += 1;
            return Some(self.board.get_single_cell(c%8,c/8));
        }
    }
}

impl IntoIterator for Board {
    type Item = u32;
    type IntoIter = BoardIter;
    fn into_iter(self) -> Self::IntoIter {
        return BoardIter {board:self,counter:0};
    }
}


#[cfg(test)]
mod tests {
    use crate::*;
    #[test]
    fn board_iterator() {
        //default board has two x's and two 0's, 2-2=0
        let board = Board::new();
        let res: i32 = board.into_iter().fold(0,|acc: i32,cell: u32| -> i32 {
            if cell == B_PLAYER1 {
                return acc+1;
            } else if cell == B_PLAYER2 {
                return acc-1;
            };
            return acc;
        });
        assert_eq!(res,0);
    }
    #[test]
    fn from_str_test() {
        let boardstr: &str = 
"
| | | | | | | | |
| | | | | | | | |
| | | | | | | | |
| | | |x|o| | | |
| | | |o|x| | | |
| | | | | | | | |
| | | | | | | | |
| | | | | | | | |
";
        let board = Board::from_str(boardstr);
        assert_eq!(board,Board::new());


            
    }
}
