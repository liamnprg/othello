#include <stdbool.h>
#include <string.h>
#include "libothello.h"



//reduce code bloat here
bool is_player_four_cell(FourCells fc, unsigned int cell_no,unsigned int player) {
	cell_no=3-cell_no;
	cell_no=cell_no*2;
	fc = fc&(0b11<<cell_no);	
	if (fc == player<<cell_no) {
		return true;
	}
	return false;
}

unsigned int get_single_cell_four_cell(FourCells fc, unsigned int cell_no) {
	cell_no=3-cell_no;
	cell_no=cell_no*2;
	//fc = fc&(0b11<<cell_no);	
	fc = (fc>>cell_no)&0b11;
	return fc;
}

unsigned int get_single_cell(Board board, unsigned int x, unsigned int y) {
	int cellno = x+y*8;
	int fourcellno = cellno/4;
	FourCells fc = board.cells[fourcellno];
	return get_single_cell_four_cell(fc,cellno%4);
}

bool is_player(Board board, unsigned int x, unsigned int y, unsigned int player) {
	int cellno = x+y*8;
	int fourcellno = cellno/4;
	FourCells fc = board.cells[fourcellno];
	return is_player_four_cell(fc,cellno%4,player);
}

void set_piece_at(Board *board, unsigned int x, unsigned int y, unsigned int player) {
	//find the number of cells forward x and y represents
	//divide by four
	//modify that cell
	int cellno = x+y*8;
	int fourcellno = cellno/4;
	FourCells *cell = &board->cells[fourcellno];
	//clear the previous bit
	*cell&=~(0b11<<(3-((cellno)%4))*2);
	//set the bit
	*cell|=player<<(3-((cellno)%4))*2;
	return;
}
Board empty_board() {
	Board board;
	memset(&board,'\0',sizeof(Board));
	return board;
}
Board new_board() {
	Board board = empty_board();
	set_piece_at(&board,3,3,B_PLAYER1);
	set_piece_at(&board,3,4,B_PLAYER2);
	set_piece_at(&board,4,3,B_PLAYER2);
	set_piece_at(&board,4,4,B_PLAYER1);
	return board;
}
