#pragma once
#include <stdbool.h>

//char [for] player {1,2}
#define C_PLAYER1 'x'
#define C_PLAYER2 'o'
//byte [for] player {1,2,null}
#define B_PLAYER1 0b01
#define B_PLAYER2 0b10
#define B_NULL 0b00

#define AI_EZY 1
#define AI_MED 2
#define AI_HRD 3 

//opposite_player=player^F_SWTICHPLAYER
#define F_SWITCHPLAYER 0b11

//two bits per cell, a char is 1 byte, 1 byte=8 bits=4 cells
typedef unsigned char FourCells;

//8x8 board=64 cells/FourCells=16
typedef struct board {
	FourCells cells[16];
} Board;


/*
 * simple bitfield: 0b[01][11][10][00]=0b01111000
 * the four different sections are each cells. By masking them
 * starting at position cell_no*2, you pull out the cell you want
 * after that, you compare with the proper field:
 * 01=player 1
 * 10=player 2
 * 00=null
 * 11=invalid (e) on the screen
*/

//is player at x,y on board
bool is_player(Board board, unsigned int x, unsigned int y, unsigned int player);
unsigned int get_single_cell(Board board, unsigned int x, unsigned int y);

//set piece at (x,y) to player on board
void set_piece_at(Board *board, unsigned int x, unsigned int y, unsigned int player);

/*
 * execute a move to x,y from player and return the number of pieces flipped
 * returns the number of pieces flipped
 * if the number of pieces flipped is zero, the board not modified
*/
unsigned int exec_move(Board *board, unsigned int x, unsigned int y, unsigned int player);

//Return value for exec_move_nomut
typedef struct {
	unsigned int retval;
	Board board;
} ExecMoveNomutRetval;

/*
 * execute a move to x,y from player and return a new board with that move
 * completed and the number of pieces flipped
*/

ExecMoveNomutRetval exec_move_nomut(Board board, unsigned int x, unsigned int y, unsigned int player);

//creates a new board with the default othello starting positions set
Board new_board();
Board empty_board();

//checks if player can make a valid move on board
bool valid_move_exists(Board board, unsigned int player);

typedef struct {
	unsigned int x;
	unsigned int o;
} CountPiecesRetval;

//counts the number of pieces on board
CountPiecesRetval count_pieces(Board board);

typedef struct {
	unsigned int x;
	unsigned int y;
} Coordinate;

