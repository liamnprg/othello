# Othello

A C implementation of the Othello game.

## What is Othello

```
x 1 2 3 4 5 6 7 8  y
| | | | | | | | | 1
| | | | | | | | | 2
| | | | | | | | | 3
| | | |x|o| | | | 4
| | | |o|x| | | | 5
| | | | | | | | | 6
| | | | | | | | | 7
| | | | | | | | | 8
```

To my surprise, most people are not aware of what Othello is. Do not worry! It is very simple. Feel free to play against the easy ai using `cargo run -- -d1` to get a feel for how the game works.

### Rules 
>Black plays first and draws a single disc and places it on a valid spot on the grid. You must place a disc on a spot that allows you to “flank” or “capture” at least one of your opponent’s discs by bordering them in a row.

>You must flip all captured discs to your color.

>To capture your opponent’s discs you must have 1 of your own pieces at the end of a row and then on your turn place a new piece at the beginning of the row. Everything in between on that row is now flipped to your color. You can only capture rows of a single color adjacent to each other; there cannot be any open space or your own discs between them or the combo is interrupted.

(Source: [howdoyouplayit.com](https://howdoyouplayit.com/othello-rules-how-do-you-play-othello/))


## Building

run `cargo build`

## Running

run `cargo run`, Consult Usage for details

## Usage

```
othello 

A terminal-based implementation of the Othello game

USAGE:
    ai [OPTIONS]

FLAGS:
    -h, --help       Print help information
    -V, --version    Print version information

OPTIONS:
    -d, --difficulty <DIFFICULTY>    [default: 0]
```



## Features 

1. Three tiers of AI (Easy(1),Medium(2),Hard(3)) implemented using GameTrees
2. Play with two human users


## Code Quality standards (C code only)

1. Only pass pointers to functions when modifying variables
2. Variable names and function names are snake_case
3. Structs are CamelCase

## Commits Welcome!

Feel free to create MR/PR's of this project, I will most likely integrate them, especially ones that satisfy criteria below

## TODO's

* Add in networking/multi-player online
* Integrate with rayon for better performance (pretty easy due to incessant use of functional programming)
* Add in a pruning function to remove cpu and memory waste
* Gui mode
* Better display options controlled via command line
* Rework how integers work (Get c to not use u32's and instead use unsigned char's aka u8's)
* Better build scripts (Custom makefiles bad)
* WOPR Mode (eats cpu/ram but plays well)
* More fine-tuning to enable difficulties above 3 (use ai_make_move_hard but just have it go to 1..n instead of 1..4)
* Better testing
* !!!Store ui.rs and main.rs in a different directory than the library code
* !!!Fix the importing system (depends on rust being a better language with better multi-target support/separating this crate into two sub-crates, one which has mutliple binary targets)
* Cross-Compiling (Probably works but untested, be sure to set CC variable so C uses the correct compiler, same with AR)

## Tips

* To play with two players remotely, combine tmux and ssh, two users can be in a tmux session at the same time
