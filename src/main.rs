extern crate ai;
extern crate clap;

use ai::binding::*;
use ai::GameState;
use clap::Clap;
use ui::*;
//use ui::Ui;

fn main() {
    //imported from c
    let mut ai: u8 = 0;
    parse_arguments(&mut ai);

    let mut gs = GameState::new(Board::new(), B_PLAYER1);
    let board: &Board = (&gs).as_ref();

    let ui = ConsoleUi {};
    ui.redraw_board(board);
    let mut c: Coordinate;

    loop {
        c = ui.get_coord_ai_inp(&gs, ai);
        if gs.board.exec_move(c.x, c.y, gs.player) > 0 {
            gs.player ^= F_SWITCHPLAYER;
            let board: &Board = (&gs).as_ref();
            ui.redraw_board(board);

            let p1_can_move = board.valid_move_exists(gs.player);
            let p2_can_move = board.valid_move_exists(gs.player ^ F_SWITCHPLAYER);

            if !p1_can_move && p2_can_move {
                gs.player ^= F_SWITCHPLAYER
            } else if !p1_can_move && !p2_can_move {
                print!("GAME OVER");
                let rv = board.count_pieces();
                println!("The total score is:\nx:{}\no:{}\n", rv.0, rv.1);
                return;
            }
        } else {
            print!("[invalid move] ");
        }
    }
}

/// A terminal-based implementation of the Othello game
#[derive(Clap, Debug)]
#[clap(name = "othello")]
pub struct ParsedArgs {
    #[clap(short, long, default_value = "0")]
    difficulty: u8,
}

fn parse_arguments(difficulty: &mut u8) -> ParsedArgs {
    let pa = ParsedArgs::parse();
    *difficulty = pa.difficulty;
    return pa;
}

mod ui;
