#pragma once

Coordinate ai_make_move_easy(Board board, unsigned char player);
Coordinate ai_make_move_med(Board board, unsigned char player);
Coordinate ai_make_move_hard(Board board, unsigned char player);

int score_for(Board board, unsigned char player);
