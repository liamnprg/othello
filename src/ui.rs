extern crate text_io;
use self::text_io::read;
use ai::binding::*;
use ai::*;
use std::io;
use std::io::Write;

///Made to future-proof this library
pub trait Ui {
    ///Get coordinate (from user) (or) ai input depending on difficulty and GameState
    fn get_coord_ai_inp(&self, gs: &GameState, difficulty: u8) -> Coordinate;
    ///Ask user for coordinates
    fn beg_coordinates(&self, gs: &GameState) -> Coordinate;
    ///Print the ai's coordinates to the screen
    fn show_ai_coords(&self, c: Coordinate);
    ///Redraw board
    fn redraw_board(&self, board: &Board);
}
pub struct ConsoleUi {}

impl Ui for ConsoleUi {
    fn get_coord_ai_inp(&self, gs: &GameState, difficulty: u8) -> Coordinate {
        let c: Coordinate;
        if gs.player == B_PLAYER1 {
            c = self.beg_coordinates(gs);
            return c;
        } else if difficulty as u32 == AI_EZY {
            c = ai_make_move_easy(gs.board.clone(), gs.player);
        } else if difficulty as u32 == AI_MED {
            c = ai_make_move_med(gs.board.clone(), gs.player);
        } else if difficulty as u32 == AI_HRD {
            c = ai_make_move_hard(gs.board.clone(), gs.player);
        } else {
            c = self.beg_coordinates(gs);
            return c;
        }
        self.print_beg_msg(gs);
        self.show_ai_coords(c);
        return c;
    }
    fn redraw_board(&self, board: &Board) {
        println!("{}", board);
    }
    fn beg_coordinates(&self, gs: &GameState) -> Coordinate {
        self.print_beg_msg(gs);
        //don't read what we've written
        io::stdout().flush().unwrap();
        return read!("{}");
    }
    fn show_ai_coords(&self, c: Coordinate) {
        //display?
        println!("{},{}\n", c.x + 1, c.y + 1);
    }
}

impl ConsoleUi {
    fn print_beg_msg(&self, gs: &GameState) {
        let mut ch = C_PLAYER1;
        if gs.player == B_PLAYER2 {
            ch = C_PLAYER2;
        }
        print!(
            "[{}:{}] Enter the coordinates player {}:",
            ch as char,
            gs.score(),
            ch as char
        );
    }
}
