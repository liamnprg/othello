use crate::binding::*;
//use crate::gametree::*;
use crate::tree::*;
use std::fmt;

//heuristisc
const CORNER: i32=10;
const EDGE: i32  =4;
const PIECE: i32 =1;

#[derive(Debug,Clone,PartialEq,Eq)]
pub struct GameState {
    pub board: Board,
    pub player: u32,
}

impl fmt::Display for GameState {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        //player can't be null in a GameState
        let c = {
            if self.player == B_PLAYER2 {
                C_PLAYER2
            } else if self.player == B_PLAYER1 {
                C_PLAYER1
            } else {
                unreachable!();
            }
        };
        write!(f, "player:{},score:{}",c as char,self.score())
    }
}

impl GameState {
    ///Creates a new GameState
    pub fn new(board: Board, player: u32) -> Self {
        return GameState {board,player};
    }
    ///Switches the self.player field
    pub fn switch_player(&mut self) {
        self.player=switch_player(self.player)
    }
    ///Scores self
    pub fn score(&self) -> i32 {
        let player = self.player;
        //positive/negative cell number
        let res: i32 = self.as_ref().into_iter().map(|cell| {
            if cell == player {
                return 1;
            } else if cell == player^F_SWITCHPLAYER {
                return -1;
            }
            0
        }).zip(0..).map(|(cell,cellno)| {
            //corner
            if is_corner(cellno) {
                return cell*CORNER;
            } else if is_edge(cellno) {
                return cell*EDGE;
            } else {
                return cell*PIECE;
            }
        }).sum();
        return res;
    }
}

impl AsRef<Board> for GameState {
    fn as_ref(&self) -> &Board {
        return &self.board;
    }
}
fn is_corner(cellno: u32) -> bool {
    let c = cellno+1;
    c == 1 || c == 8 || c == 8*8-7 || c == 8*8
}

fn is_edge(cellno: u32) -> bool {
    if is_corner(cellno) {
        return false;
    }
    //first row
    if cellno < 8 {
        return true;
    }
    //last row
    if cellno > 63-8 {
        return true;
    }
    //last column
    if (cellno+1)%8 == 0 {
        return true;
    }
    //first column
    if (cellno+1)%8 == 1 {
        return true;
    }
    false
}


///Can be called from C
#[no_mangle]
pub extern fn ai_make_move_easy(board: Board, player: u32) -> Coordinate {
    //pick a direction and make every move starting with that direction. Return the first valid
    //move
    let mut i = 0;
    let mut end = 8*8;
    if rand::random() {
        i = 8*8;
        end = 0;
    }
    while i != end {
        let x = i%8;
        let y = i/8;
        let rv = unsafe {exec_move_nomut(board,x,y,player)};
        if rv.retval > 0 {
            let c = Coordinate { x:x,y:y};
            return c;
        }
        if i == end {
        } else if i > end {
            i-=1;
        } else {
            i+=1;
        }
    }
    //valid_move_exists would have caught that there are no valid moves
    unreachable!();
}

///Can be called from C
#[no_mangle]
pub extern fn score_for(board: Board, player: u32) -> i32 {
    GameState::new(board,player).score()
}

///Can be called from C
#[no_mangle]
pub extern fn ai_make_move_med(board: Board, player: u32) -> Coordinate {
    //the board state was created by the previous player, so that is who we set the player to
    let mut gt = GameTreeNode::from_parts(board,player^F_SWITCHPLAYER);
    gt.make_all_moves();
    let gs: GameTreeNode = gt.find_best_game_state();
    return gs.move_made;
}

///Can be called from C
#[no_mangle]
pub extern fn ai_make_move_hard(board: Board, player: u32) -> Coordinate {
    //algorithm:
    //  make all moves some number of times to build out the game tree
    //  if implementing higher difficulties, implement some kind of pruning to keep the tree within
    //      some kind of bounds
    //  find best game state by aggregating the branches
    let mut gt = GameTreeNode::from_parts(board,switch_player(player));
    for _ in 1..4 {
        gt.make_all_moves_on_leaves();
    }
    //println!("{}",gt);
    let gs: GameTreeNode = gt.find_best_game_state();
    return gs.move_made;
}

///Switches a player
pub fn switch_player(player: u32) -> u32 {
    player^(F_SWITCHPLAYER as u32)
}

#[cfg(test)]
mod tests {
    use crate::*;
    #[test]
    fn switch_player_t() {
        let player = B_PLAYER1;
        assert_eq!(switch_player(player),B_PLAYER2);
        assert_eq!(switch_player(switch_player(player)),B_PLAYER1);
    }
    #[test]
    fn is_corner_t() {
        let mut board = Board::empty();
        let boardstr2: &str = 
"
|o| | | | | | |o|
| | | | | | | | |
| | | | | | | | |
| | | | | | | | |
| | | | | | | | |
| | | | | | | | |
| | | | | | | | |
|o| | | | | | |o|
";
        let board2 = Board::from_str(boardstr2);
        for i in 0..64 {
            if is_corner(i) {
                board.set_piece_at(i%8,i/8,B_PLAYER2)
            }
        }
        println!("{}",board);
        println!("{}",board2);
        assert_eq!(board,board2)

    }
    //24 edges
    #[test]
    fn is_edge_t() {
        let boardstr: &str = 
"
| |x|x|x|x|x|x| |
|x| | | | | | |x|
|x| | | | | | |x|
|x| | | | | | |x|
|x| | | | | | |x|
|x| | | | | | |x|
|x| | | | | | |x|
| |x|x|x|x|x|x| |
";
        let board2 = Board::from_str(boardstr);
        let mut board = Board::empty();
        for i in 0..64 {
            if is_edge(i) {
                board.set_piece_at(i%8,i/8,B_PLAYER1)
            }
        }
        println!("{}",board);
        println!("{}",board2);
        assert_eq!(board,board2)

    }
    #[test]
    fn score_t() {
        let boardstr: &str = 
"
|o|x|x|x|x|x|x|o|
|x| | | | | | |x|
|x| | | | | | |x|
|x| | |o|o| | |x|
|x| | |o|o| | |x|
|x| | | | | | |x|
|x| | | | | | |x|
|o|x|x|x|x|x|x|o|
";
        let board = Board::from_str(boardstr);
        let gs = GameState::new(board,B_PLAYER1);
        assert_eq!(gs.score(),(6*4*EDGE)-(4*CORNER)-4*PIECE)
    }
}

pub mod binding;
pub mod gametree;
pub mod tree;
