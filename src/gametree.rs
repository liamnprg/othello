use crate::*;


impl GameTreeNode {
    ///Make all the opponent's moves and append them as children
    ///does nothing if self is not a leaf node
    pub fn make_all_moves(&mut self) {
        if self.is_leaf() {
            for i in 0..64 {
                self.branch_move(i%8,i/8);
            }
        }
    }
    ///Finds best game state in children, not recursive
    pub fn find_best_game_state_old(&self) -> GameTreeNode {
        let gtns = self.leaves();
        let gtns2 = gtns.clone();
        let score_and_gtn: Vec<(i32,GameTreeNode)> = gtns.into_iter()
        //bug here? according to who is the scoring done?
        .map(|gtn| gtn.gs.score())
        .zip(gtns2).collect();
        let best_gtn = score_and_gtn.into_iter().max_by(|(score,_),(score2,_)| score.cmp(score2)).unwrap();
        //unwrap is safe here due to the valid_move_exists call;
        //let best_gtn = score_and_gtn.max().unwrap().1;
        return best_gtn.1;
    }
    ///Seeks leaves of root node and makes all moves for each leaf
    pub fn make_all_moves_on_leaves(&mut self) {
        self.make_all_moves();
        (*self.children).iter_mut()
        .map(|gtn| {
            if gtn.is_leaf() {
                gtn.make_all_moves();
            } else {
                (*gtn.children).iter_mut().map(|cgtn| cgtn.make_all_moves_on_leaves()).for_each(drop);
            }
        }).for_each(drop);
    }
    ///Creates an aggregate value for each child of self which is a sum of each branch's score
    pub fn aggregate_children(&self) -> i32 {
        //algorithm:
        //  if aggregating a leaf:
        //      return score(self)
        //  if aggregating a branch:
        //  sum sub-branches
        let res: i32 = -(*self.children).into_iter()
        .map(|gtn| {
            if gtn.is_leaf() {
                return gtn.gs.score();
            } else {
                //aggregating for opposite player
                return -(*gtn.children).into_iter().map(|cgtn| cgtn.aggregate_children()).sum::<i32>();
            }
        }).sum::<i32>();
        return res;
    }
    ///Finds which child produces the best aggregate game state
    pub fn find_best_game_state(&self) -> GameTreeNode {
        //algorithm:
        //  aggregate child for each child
        //  zip with its gtn
        //  find max
        //  return max
        let res:&GameTreeNode = (*self.children).into_iter()
        .map(|gtn| {
            return gtn.aggregate_children();
        })
        .zip(&self.children)
        .max_by(|(score,_),(score2,_)| score.cmp(score2)).unwrap().1;
        return res.clone();
    }
}

#[cfg(test)]
mod tests {
    use crate::*;
    #[test]
    fn aggregate_children_t() {
        let mut root = GameTreeNode::from_parts(Board::new(),B_PLAYER1);
        for _ in 0..1 {
            root.make_all_moves_on_leaves();
        }
        let branches: Vec<i32> = (*root.children).into_iter().map(|gtn| {
            gtn.aggregate_children()
        }).collect();
        println!("{}",root);
        //known tree
        assert_eq!(branches,vec![0,0,0,0]);
    }
    #[test]
    fn find_best_game_state_t() {
        let boardstr: &str = 
"
| |x|x|x| |o|o|o|
| | |x| | | |o| |
|x| |x|x|x|x|x| |
|o|x|x|x|x|x| | |
| | |x|x|x|x|x| |
| | |x| | | | | |
| |o|x| | | | | |
|o| | | | | | | |
";
        let board = Board::from_str(boardstr);
        let mut root = GameTreeNode::from_parts(board,B_PLAYER1);
        for _ in 0..1 {
            root.make_all_moves_on_leaves();
        }

        let best_game_state = root.find_best_game_state();
        println!("{}",root);
        //known tree
        assert_eq!(best_game_state.move_made,Coordinate::new(6,3));
    }
}
