//trees rust-lang library is too confusing to use
//petgraph was also hard
use std::fmt;
//use binding::*;
use std::fmt::Display;
use crate::*;



#[derive(Debug,Clone,PartialEq,Eq)]
pub struct GameTreeNode {
    pub gs: GameState,
    pub move_made: Coordinate,
    pub children: Vec<GameTreeNode>,
}

impl fmt::Display for GameTreeNode {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.fmt_inner(f,1)
    }
}


impl GameTreeNode {
    ///recursive display implementation
    fn fmt_inner(&self,f:&mut fmt::Formatter,level:usize) -> fmt::Result {
        self.gs.fmt(f)?;
        write!(f, ",move_made: {},aggregate: {}\n",self.move_made,self.aggregate_children())?;
        Ok(for child in &self.children {
            for _ in 0..level {
                write!(f,"  ")?
            }
            child.fmt_inner(f,level+1)?;
        })
    }
    ///Creates a new GameTreeNode with no children
    pub fn new(gs:GameState, move_made: Coordinate) -> Self {
        return GameTreeNode {gs,move_made,children:Vec::new()};
    }
    ///Number of children is zero
    pub fn is_leaf(&self) -> bool {
        return self.children.len() == 0;
    }
    ///Returns a copy of all leaf nodes of root node `self`
    pub fn leaves(&self) -> Vec<GameTreeNode> {
        if self.is_leaf() {
            return vec!(self.clone());
        } else {
            return (&self.children).into_iter().map(|x| x.leaves()).flatten().collect();
        }
    }
    ///Creates a new GameTreeNode with no children and a null coordinate set 
    pub fn from_parts(board:Board,player:u32) -> Self {
        return GameTreeNode{gs: GameState::new(board,player),move_made: Coordinate::null(),children:Vec::new()};
    }

    ///Tries to execute move to (x,y) as the opposite player to self.player, adds that move as a
    ///child GameTreeNode if the move is valid
    pub fn branch_move(&mut self, x: u32, y:u32) {
        //switches player for new_gs
        let (new_gs,rv) = self.gs.board.exec_move_nomut(x,y,switch_player(self.gs.player));
        if rv > 0 {
            let move_made = Coordinate::new(x,y);
            self.children.push(GameTreeNode::new(new_gs,move_made));
        } 
    }
}

#[cfg(test)]
mod tests {
    use crate::*;
    /*#[test]
    fn switch_player_t() {
        let player = B_PLAYER1;
        assert_eq!(switch_player(player),B_PLAYER2);
    }*/
}
